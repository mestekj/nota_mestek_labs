function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Collect metal in given area",
        parameterDefs = {
            { 
                name = "farckIDs",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "area",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end


function Run(self, units, parameter)
    -- Once running (always): 
    -- FAIL CONDITIONS
    -- No living Farck
    local anyLiving = false
    for idx, unitID in ipairs(parameter.farckIDs) do
        if Spring.ValidUnitID(unitID) and UnitDefs[Spring.GetUnitDefID(unitID)].name == "armfark" then
            anyLiving = true
        end
    end

    if not anyLiving or #parameter.farckIDs == 0 then
        return FAILURE
    end

    -- SUCCESS CONDITIONS
    -- No remaining metal (with some tolerance)
    if Sensors.nota_mestek_labs.MetalInArea(parameter.area) < 10 then
        return SUCCESS
    end

    -- first time
    if not self.initialized then
        for _, unitID in ipairs(parameter.farckIDs) do
            Spring.GiveOrderToUnit(unitID, CMD.RECLAIM,ToSpringArea(parameter.area),{})
        end
        self.initialized = true
    end

    return RUNNING
end

function ToSpringArea(area)
    return {area.center.x, area.center.y, area.center.z, area.radius}
end

function Reset(self)
    self.initialized = false
end