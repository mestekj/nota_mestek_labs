function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Load a given unit",
        parameterDefs = {
            { 
                name = "unitsIDs",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "targetUnitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

--[[
Node name: Load unit
Parameters
    unitID we would like to be rescued
    unitID of the transporter
Definition of SUCCESS: 
    “Unit to be rescued” is loaded in “transporter”
Definition of FAIL: 
    “Unit to be rescued” is dead
    “Unit to be rescued” is loaded by other trasnporter
    “Unit to be rescued” is not transportable
    “Transporter” is dead
    “Transporter” is not transporter
    “They are far away from each other”
    Unit stops to execute “Spring” order
Definition of RUNNING: 
    Not success, Not fail, 
Init (only first time): 
    Check all failure conditions
    Give “Spring” order to transporter to load a unit
Once running (always): 
    Check all failure conditions
    Check all success conditions
]]--

function Run(self, units, parameter)

    if #parameter.unitsIDs == 0 then
        return FAILURE
    end

    if parameter.targetUnitID == nil then
        return FAILURE
    end

    if not Spring.ValidUnitID(parameter.targetUnitID) then
        return SUCCESS
    end
    
   
    -- first time
    if not self.initialized then
        for _, unitID in ipairs(parameter.unitsIDs) do
            Spring.GiveOrderToUnit(unitID, CMD.FIRE_STATE,{0},{})
            Spring.GiveOrderToUnit(unitID, CMD.ATTACK,{parameter.targetUnitID},{})
        end
        self.initialized = true
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end