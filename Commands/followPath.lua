function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move along given path",
        parameterDefs = {
            { 
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "path",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

local THRESHOLD = 200

function Run(self, units, parameter)
    -- Once running (always): 
    -- FAIL CONDITIONS
    -- “Unit” is dead
    if not Spring.ValidUnitID(parameter.unitID) then
        return FAILURE
    end
    -- “They are far away from each other”
    -- Unit stops to execute “Spring” order
    -- https://trello.com/c/qY9BfW2L/49-how-to-manipulate-with-command-queue
    if parameter.path == nil or #parameter.path == 0 then
        return FAILURE
    end

    local pointX, pointY, pointZ = Spring.GetUnitPosition(parameter.unitID)
	local unitPosition = Vec3(pointX, pointY, pointZ)

    -- SUCCESS CONDITIONS
    -- Check all success conditions
    if unitPosition:Distance(parameter.path[#parameter.path]) < THRESHOLD then
        return SUCCESS
    end

    -- first time
    if not self.initialized then
        for _,position in ipairs(parameter.path) do
            Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE,position:AsSpringVector(),{"shift"})
        end
        self.initialized = true
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end