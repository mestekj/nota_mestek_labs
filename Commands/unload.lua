function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Unload a unit to given area",
        parameterDefs = {
            { 
                name = "unitTransportID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "areaToUnloadTo",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

--[[
Node name: Load unit
Parameters
    unitID of the transporter
    area where to unload
Definition of SUCCESS: 
    Unit carried by "tranporter" is unloaded into “area”
    "Transporter" is empty ??
Definition of FAIL: 
    “Transporter” is dead
    “Transporter” is not transporter
    Unit stops to execute “Spring” order
Definition of RUNNING: 
    Not success, Not fail, 
Init (only first time): 
    Check all failure conditions
    Give “Spring” order to transporter to load a unit
Once running (always): 
    Check all failure conditions
    Check all success conditions
]]--

sizeNeeded = 200
free_places = nil

function Run(self, units, parameter)
    -- Once running (always): 
    -- FAIL CONDITIONS
    -- “Transporter” is dead
    if not Spring.ValidUnitID(parameter.unitTransportID) then
        Logger.warn("Unload","UnitTransportID is dead.")
        return FAILURE
    end
    -- “Transporter” is not transporter
    if not UnitDefs[Spring.GetUnitDefID(parameter.unitTransportID)].isTransport then
        Logger.warn("Unload","UnitTransportID is not transporter.")
        return FAILURE
    end
    -- “They are far away from each other”
    -- Unit stops to execute “Spring” order
    -- https://trello.com/c/qY9BfW2L/49-how-to-manipulate-with-command-queue

    -- SUCCESS CONDITIONS
    -- Check all success conditions
    if #Spring.GetUnitIsTransporting(parameter.unitTransportID) == 0 then
        return SUCCESS
    end  

    -- first time
    -- if not self.initialized then
    --     Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.UNLOAD_UNITS,ToSpringArea(parameter.areaToUnloadTo),{"shift"})
    --     self.initialized = true
    -- end


    if free_places == nil then
        free_places = FindFreePlaces(parameter.areaToUnloadTo)
    end

    -- find new free place
    if not self.initialized  then
        self.x, self.z = FindFreePlace()

        -- no free place to unload unit
        if self.x == nil or self.z == nil then
            Logger.warn("Unload", "There is no free place to unload unit.")
            return FAILURE
        end

        Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.MOVE,{self.x, 0, self.z},{})
        -- Logger.warn("Unload initialized")
        self.initialized = true
        self.waitOnPosition = 4
        self.stopOrderGiven = false
        self.unloadOrderGiven = false
    end

    

    local posX, posY, posZ = Spring.GetUnitPosition(parameter.unitTransportID)
    local distanceToDestination = (Vec3(posX,posY,posZ) - Vec3(self.x, posY, self.z)):Length()
    
    -- Logger.warn(string.format("distanceToDestination %f", distanceToDestination))

    if distanceToDestination < sizeNeeded then
        self.waitOnPosition = self.waitOnPosition - 1
        if not self.stopOrderGiven then
            Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.STOP,{},{})
            -- Logger.warn("Unload", "Stop order given.")
            self.stopOrderGiven = true
        end
    end

    if self.waitOnPosition <= 0 and not self.unloadOrderGiven then
        Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.UNLOAD_UNIT,{posX, posY, posZ},{})
        self.unloadOrderGiven = true
        -- Logger.warn("Unload", "UnloadUnit order given.")
    end

    return RUNNING
end

function ToSpringArea(area)
    return {area.center.x, area.center.y, area.center.z, area.radius}
end

function Reset(self)
    self.initialized = false
end


function FindFreePlace()
    if #free_places > 0 then
        local place = free_places[#free_places]
        table.remove(free_places, #free_places)
        return place.x, place.z
    else
        return nil, nil -- no free place in area
    end
end


function FindFreePlaces(area)
    local places = {}
    for x_delta = -area.radius, area.radius, sizeNeeded/2 do
        for z_delta = -area.radius, area.radius, sizeNeeded/2 do
            if x_delta^2 + z_delta^2 <= area.radius^2 then -- is in area
                local x = area.center.x + x_delta
                local z = area.center.z + z_delta
                table.insert(places, {x = x, z = z})
            end
        end
    end
    return places
end