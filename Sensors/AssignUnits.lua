local sensorInfo = {
	name = "AssignUnits",
	desc = "Assign (list of) unit(s) to given group.",
	author = "Jakub Mestek",
	date = "2021-07-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description assign unit
return function(unitIDs, groupID, groups,groupAssignment)
	if type(unitIDs) ~= "table" then
		unitIDs = {unitIDs}
	end

	if groups[groupID] == nil then
		groups[groupID] = {}
	end

	for _, unitID in ipairs(unitIDs) do
		--table.insert(groups[groupID], unitID)
		local group = groups[groupID]
		group[#group+1] = unitID
		groupAssignment[unitID] = groupID
	end
end