local sensorInfo = {
	name = "BattleLine",
	desc = "Returns 1d coordinate of battle-line in given corridor.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local From1D = Sensors.nota_mestek_labs.From1D
local To1D = Sensors.nota_mestek_labs.To1D
local EnemyUnitsInArea = Sensors.nota_mestek_labs.EnemyUnitsInArea

-- @description return battle-line coordinates
-- battle-line is defined as nearest point to my base where are enemies
return function(path, radius)
	local radius = radius or 600
	local step = radius/2

	local coridorEnd1d = To1D(path, path[#path])
    for center = 0, math.floor(coridorEnd1d), step do
        local enemies = EnemyUnitsInArea({center = From1D(path, center), radius=radius})
		if #enemies > 0 then
			return center + step -- return aprox. position of units, not center of first circle that touches them
		end
    end
    return coridorEnd1d
end