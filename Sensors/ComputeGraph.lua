local sensorInfo = {
	name = "Compute graph",
	desc = "Finds safe path.",
	author = "Jakub Mestek",
	date = "2021-07-18",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local gridSize = 50
local hStrength = 0.8

local mapWidth = Game.mapSizeX
local mapHeight = Game.mapSizeZ

local SpringGetGroundHeight = Spring.GetGroundHeight

grid = nil -- to have unique nodes

function populateGrid()
    grid = {}
    for x = 0, mapWidth, gridSize do
        grid[x] = {}
        for z = 0, mapHeight, gridSize do
            grid[x][z] = {x=x,z=z}
        end
    end
end
 
function getHeight(pos)
    return SpringGetGroundHeight(pos.x,pos.z)
end

function getNearestGridPoint(pos)
    return toNode({x=gridSize * math.floor(pos.x/gridSize), z= gridSize * math.floor(pos.z/gridSize)})
end

function toNode(pos)
    return grid[pos.x][pos.z]
end

function toPos(node)
    return node
end


local dxs = {-gridSize, 0, gridSize, 0}
local dzs = {0, -gridSize, 0, gridSize}



function getNeighbours(node)
    local pos = toPos(node)
    local neighbours = {}
    for i, dx in ipairs(dxs) do
        local dz = dzs[i]
        local nx = pos.x + dx
        local nz = pos.z + dz
        
        if (nx > 0) and (nz > 0) and (nx < mapWidth) and (nz < mapHeight) then
            table.insert(neighbours, toNode({x=nx,z=nz}))
        end
    end
    return neighbours
end

function filter(array, predicate)
    result = {}
    for _,val in ipairs(array) do
        if predicate(val) then table.insert(result, val) end
    end
    return result
end


function bfs(start, cost)
    -- ALL "POS" ARE IN FACT NODES 

    local closedVertices = {}
    local queue = { start }
    local labels = { [start] = nil }
    local f = { [start] = 0 }

    while #queue > 0 do
        local min_pos = queue[1]
        table.remove(queue,1)

        if closedVertices[min_pos] == nil then
            closedVertices[min_pos] = true

            local neighbours = filter(getNeighbours(min_pos), function (pos) return not closedVertices[pos] end)
            local min_pos_f = f[min_pos]
            for _,neighbour in ipairs(neighbours) do
                local nd = min_pos_f + gridSize + cost(neighbour)
                if f[neighbour] == nil or nd < f[neighbour] then
                    f[neighbour] = nd
                    labels[neighbour] = min_pos
                    table.insert(queue, neighbour)
                end
            end
        end
    end
    -- Logger.warn("No path found.")
    return labels
end

function dangerousness(node)
    return hStrength*getHeight(toPos(node))
end


-- @description return path from startPos to targetPos
return function(safePos, custom_hStrength)
    hStrength = custom_hStrength or hStrength

    if grid == nil then
        populateGrid()
        -- Logger.warn("Grid populated.")
    end
    
    local start = getNearestGridPoint(safePos)
    
    labels = bfs(start, dangerousness)

    return {grid = grid, labels = labels, gridSize = gridSize}
end