local sensorInfo = {
	name = "Coordinate1DDebug",
	desc = "Draws an X at given coordinate.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function drawX(position3d, size, key)
	local s = size/2
	Script.LuaUI.exampleDebug_update(
		key.."l1", -- key
		{	-- data
			startPos = position3d + Vec3(-s,0, -s), 
			endPos =position3d + Vec3(s,0,s)
		}
	)

	Script.LuaUI.exampleDebug_update(
		key.."l2", -- key
		{	-- data
			startPos = position3d + Vec3(s,0, -s), 
			endPos = position3d + Vec3(-s,0, s)
		}
	)
end

-- @description draw X at given position
return function(path, pos1d, key)
	if (not Script.LuaUI('exampleDebug_update')) then
		return false
	end

	local pos3d = Sensors.nota_mestek_labs.From1D(path, pos1d)
	drawX(pos3d, 300, key)
end