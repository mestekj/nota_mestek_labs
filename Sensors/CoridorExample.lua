local sensorInfo = {
	name = "CoridorExample",
	desc = "Returns a corridor.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local From1D = Sensors.nota_mestek_labs.From1D
local To1D = Sensors.nota_mestek_labs.To1D

-- @description return battle-line coordinates
-- battle-line is defined as nearest point to my base where are enemies
return function() 
	local coridor = { Vec3(3000, 368, 3000 ), Vec3(3300, 368, 4700 ), Vec3(4700, 368, 5900 ), Vec3(6500, 368, 6500 )} 
	return coridor
end