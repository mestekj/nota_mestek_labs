moduleInfo = {
	name = "DrawArrow",
	desc = "Draws an arrow",
	author = "Jakub Mestek",
	date = "2021-05-17",
	license = "MIT",
	layer = -1,
	enabled = true
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function computeArrow(a, b)

	local headSize = 0.1

	local base = a * headSize + b * (1-headSize)
	local dir = b-base
	local perp = Vec3(-dir.z, 0, dir.x)
	local c1 = base + perp
	local c2 = base - perp
	return {from=a, to=b, h1 = c1, h2=c2}
end

return function(key, data)
	if (not Script.LuaUI('exampleDebug_update')) then
		return false
	end

	local from = data.startPos
	local to = data.endPos
	local arrow = computeArrow(from, to)

	Script.LuaUI.exampleDebug_update(
		key.."t", -- key
		{	-- data
			startPos = arrow.from, 
			endPos = arrow.to
		}
	)
	Script.LuaUI.exampleDebug_update(
		key.."h1", -- key
		{	-- data
			startPos = arrow.h1, 
			endPos = arrow.to
		}
	)
	Script.LuaUI.exampleDebug_update(
		key.."h2", -- key
		{	-- data
			startPos = arrow.h2, 
			endPos = arrow.to
		}
	)

	return true
end