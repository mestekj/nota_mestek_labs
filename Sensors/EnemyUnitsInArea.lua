local sensorInfo = {
	name = "EnemyUnitsInArea",
	desc = "Return list of enemy units in given area.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


-- @description return list of enemy units in area
return function(area)
	local enemyTeams = Sensors.core.EnemyTeamIDs()
	local enemyUnits = {}
	
	for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetUnitsInCylinder(area.center.x, area.center.z, area.radius,teamID)
		
		for u=1, #thisTeamUnits do
			enemyUnits[#enemyUnits + 1] = thisTeamUnits[u]
		end
	end
	
	return enemyUnits
end