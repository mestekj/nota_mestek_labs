local sensorInfo = {
	name = "Filter",
	desc = "Filter lua table (array).",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return filter function
return function(array, predicate)
	local result = {}
    for _,val in ipairs(array) do
        if predicate(val) then table.insert(result, val) end
    end
    return result
end