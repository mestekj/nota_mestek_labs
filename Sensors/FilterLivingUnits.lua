local sensorInfo = {
	name = "FilterLivingUnits",
	desc = "Filter list of units.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringValidUnitID = Spring.ValidUnitID

-- @description return filter function
return function(unitsIDs)
	local toRemove = {}
    for idx,val in ipairs(unitsIDs) do
        if not SpringValidUnitID(val) then table.insert(toRemove, idx) end
    end
    for i = #toRemove, 1, -1 do
        table.remove(unitsIDs, toRemove[i])
    end
	return {invalidIdx = toRemove, filtered = unitsIDs}
end