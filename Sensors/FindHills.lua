local sensorInfo = {
	name = "WindDebug",
	desc = "Draws an arrow showing wind strength and direction.",
	author = "Jakub Mestek",
	date = "2021-05-17",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local hillSize = 100

function isNewHill(pos,hills)
    for _, hill in ipairs(hills) do
        if (hill - pos):Length() < 2*hillSize then 
            return false
        end
    end
    return true
end

function getPrecisePosition(pos, threshold)
    local minX, maxX, minZ,maxZ = math.huge,0, math.huge, 0
    for x = pos.x-hillSize,pos.x+hillSize,hillSize/10 do
        local y = Spring.GetGroundHeight(x,pos.z)
        if y > (threshold + pos.y)/2 then
            if x < minX then minX = x end
            if x > maxX then maxX = x end
        end
    end

    for z = pos.z-hillSize,pos.z+hillSize,hillSize/10  do
        local y = Spring.GetGroundHeight(pos.x,z)
        if y > (threshold + pos.y)/2 then
            if z < minZ then minZ = z end
            if z > maxZ then maxZ = z end
        end
    end

    local x, z = (minX + maxX)/2, (minZ+maxZ)/2
    return Vec3(x, Spring.GetGroundHeight(x,z), z)
end

-- @description return hills, each hill is represented by one position in the middle of the hill
function getHills(threshold)
    local hills = {}
    local mapWidth = Game.mapSizeX
    local mapHeight = Game.mapSizeZ

    for x = 0, mapWidth, hillSize do
        for z = 0, mapHeight, hillSize do
            local y = Spring.GetGroundHeight(x,z)
            if y > threshold then
                local pos = Vec3(x,y,z)
                if isNewHill(pos,hills, threshold) then
                    table.insert(hills, getPrecisePosition(pos, threshold))
                end
            end
        end
    end
    return hills
end

function isGuarded(hill,enemyPositions)
    for _, pos in ipairs(enemyPositions) do
        if (hill - pos):Length() < 2*hillSize then 
            return true
        end
    end
    return false
end



-- @description return hills, each hill is represented by one position in the middle of the hill
return function(threshold, enemyPositions)
    local hills = getHills(threshold)
    local guarded = {}
    local unguarded = {}

    for _, hill in ipairs(hills) do
        if isGuarded(hill, enemyPositions) then 
            table.insert(guarded, hill)
        else
            table.insert(unguarded, hill)
        end
    end
    return {
        guarded = guarded,
        unguarded = unguarded
    }
end