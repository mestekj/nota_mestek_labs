local sensorInfo = {
	name = "FindMetal",
	desc = "Finds best area where to collect metal in given coridor.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local MetalInArea = Sensors.nota_mestek_labs.MetalInArea
local From1D = Sensors.nota_mestek_labs.From1D

-- @description return best area where to collect iron
return function(path, currPos1D, limit1D, radius)
	local radius = radius or 600
	local step = radius

	local cmax = 0
    local cmax_metal = 0
    for center = 0, math.floor(limit1D), step do
        local metal = MetalInArea({center = From1D(path, center), radius = radius})
		local distance = math.abs(currPos1D - center)
		metal = metal / (distance / 1000) -- penalization
        
		if metal > cmax_metal then
            cmax_metal = metal
            cmax = center
        end
    end
	local area = {center = From1D(path, cmax), radius = radius}
	return {area = area, center1d = cmax}
end