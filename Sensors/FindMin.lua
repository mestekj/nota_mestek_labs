local sensorInfo = {
	name = "FindMin",
	desc = "Find minimum in lua array.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return findMin function
return function(array, key_fn)
	local cmin = nil
    local cmin_key = math.huge
    local cmin_index = nil
    for i, val in ipairs(array) do
        local k = key_fn(val)
        Logger.warn("FindMin", "k = "..k)
        if k < cmin_key then
            cmin_key = k
            cmin = val
            cmin_index = i
        end
    end
    return cmin_index, cmin
end