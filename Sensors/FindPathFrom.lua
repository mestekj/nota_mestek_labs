local sensorInfo = {
	name = "FindPathTo",
	desc = "Finds safe path.",
	author = "Jakub Mestek",
	date = "2021-07-18",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local SpringGetGroundHeight = Spring.GetGroundHeight

function getNearestGridPoint(pos)
    local gridSize = bb.graph.gridSize
    return toNode({x=gridSize * math.floor(pos.x/gridSize), z= gridSize * math.floor(pos.z/gridSize)})
end

function toNode(pos)
    return bb.graph.grid[pos.x][pos.z]
end

function toPos(node)
    return node
end

function getHeight(pos)
    return SpringGetGroundHeight(pos.x,pos.z)
end


-- @description return path from startPos to targetPos
return function(startPos, targetPos)
    local labels = bb.graph.labels
    
    local start = getNearestGridPoint(startPos)
    local target = getNearestGridPoint(targetPos)

    local node = start
    path = {}

    while labels[node] ~= nil do
        node = labels[node]
        pos = toPos(node)
        table.insert(path, Vec3(pos.x, getHeight(pos), pos.z))
    end

    return path
end