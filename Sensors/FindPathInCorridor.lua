local sensorInfo = {
	name = "FindPathInCorridor",
	desc = "Return path between two 1d coords through given corridor.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- return index of last node of path that is before pos1d
function getLastIndex(path, pos1d)
	local remains = pos1d
	for i = 2, #path do
		local stepLength = (path[i] - path[i-1]):Length()
		remains = remains - stepLength
		if remains < 0 then
			return i-1
		end
	end
	return #path
end

-- @description return path between form1d and to1d
return function(path, from1d, to1d)
	local first = nil 
	local last = nil
	local step = 0
	if from1d < to1d then
		first = getLastIndex(path, from1d) + 1 + 1
		last = getLastIndex(path, to1d)
		step = 1
	else -- +/-1 in first is to skip first point on returned path
		first = getLastIndex(path, from1d) - 1
		last = getLastIndex(path, to1d) + 1
		step = -1
	end

	local p = {}
	for i = first, last, step do
		table.insert(p, path[i])
	end
	table.insert(p, Sensors.nota_mestek_labs.From1D(path, to1d))
	return p
end