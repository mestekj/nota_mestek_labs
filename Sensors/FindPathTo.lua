local sensorInfo = {
	name = "FindPathTo",
	desc = "Finds safe path.",
	author = "Jakub Mestek",
	date = "2021-07-18",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return path from startPos to targetPos
return function(startPos, targetPos)
    local path_reversed = Sensors.nota_mestek_labs.FindPathFrom(targetPos, startPos, labels)

    local path = {}

    for i = #path_reversed, 1, -1 do
        table.insert(path, path_reversed[i])
    end

    return path
end