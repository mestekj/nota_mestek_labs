local sensorInfo = {
	name = "FindSfvePath",
	desc = "Finds safe path.",
	author = "Jakub Mestek",
	date = "2021-07-18",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local gridSize = 100
local hStrength = 0.8
local dropRate = 1

local mapWidth = Game.mapSizeX
local mapHeight = Game.mapSizeZ

grid = nil -- to have unique nodes

function populateGrid()
    grid = {}
    for x = 0, mapWidth, gridSize do
        grid[x] = {}
        for z = 0, mapHeight, gridSize do
            grid[x][z] = {x=x,z=z}
        end
    end
end
 
function getHeight(pos)
    return Spring.GetGroundHeight(pos.x,pos.z)
end

function getNearestGridPoint(pos)
    return toNode({x=gridSize * math.floor(pos.x/gridSize), z= gridSize * math.floor(pos.z/gridSize)})
end

function toNode(pos)
    return grid[pos.x][pos.z]
end

function toPos(node)
    return node
end

function filter(array, predicate)
    result = {}
    for _,val in ipairs(array) do
        if predicate(val) then table.insert(result, val) end
    end
    return result
end

function findMin(array, key_fn)
    local cmin = nil
    local cmin_key = math.huge
    local cmin_index = nil
    for i, val in ipairs(array) do
        k = key_fn(val)
        if k < cmin_key then
            cmin_key = k
            cmin = val
            cmin_index = i
        end
    end
    return cmin_index, cmin
end

local dxs = {-gridSize, 0, gridSize, 0}
local dzs = {0, -gridSize, 0, gridSize}



function getNeighbours(node)
    local pos = toPos(node)
    local neighbours = {}
    for i, dx in ipairs(dxs) do
        local dz = dzs[i]
        local nx = pos.x + dx
        local nz = pos.z + dz
        
        if (nx > 0) and (nz > 0) and (nx < mapWidth) and (nz < mapHeight) then
            table.insert(neighbours, toNode({x=nx,z=nz}))
        end
    end
    return neighbours
end


function astar(start, target, h, cost)
    -- ALL "POS" ARE IN FACT NODES 

    local closedVertices = {}
    local queue = { start }
    local labels = { [start] = nil }
    local f = { [start] = 0 }

    while #queue > 0 do
        local i, min_pos = findMin(queue, function (pos) return f[pos] + h(pos,target) end)
        table.remove(queue,i)
        if closedVertices[min_pos] == nil then
            closedVertices[min_pos] = true

            local neighbours = filter(getNeighbours(min_pos), function (pos) return not closedVertices[pos] end)
            local min_pos_f = f[min_pos]
            for _,neighbour in ipairs(neighbours) do
                local nd = min_pos_f + gridSize + cost(neighbour)
                if f[neighbour] == nil or nd < f[neighbour] then
                    f[neighbour] = nd
                    labels[neighbour] = min_pos
                    table.insert(queue, neighbour)
                end
            end
        end

        if min_pos == target then
            -- reconstruct path
            local path = {}
            local cpos = target
            while cpos ~= start do
                table.insert(path,cpos)
                cpos = labels[cpos]
            end
            table.insert(path, start)

            return processPath(path)
        end
    end
    -- Logger.warn("No path found.")
end

function manhattan(n1,n2)
    local p1 = toPos(n1)
    local p2 = toPos(n2)
    return math.abs(p1.x - p2.x) + math.abs(p1.z - p2.z)
end

function dangerousness(node)
    return hStrength*getHeight(toPos(node))
end

-- reverse and map nodes to Vec3
function processPath(path)
    local newPath = {}

    for i = #path, 1, -1 do
        if i == #path or i == 1 or i % dropRate == 0 then
            local pos = toPos(path[i])
            table.insert(newPath, Vec3(pos.x, getHeight(pos), pos.z))
        end
    end
    
    return newPath
end


-- @description return path from startPos to targetPos
return function(startPos, targetPos,  custom_hStrength, custom_dropRate)
    hStrength = custom_hStrength or hStrength
    dropRate = custom_dropRate or dropRate

    if grid == nil then
        populateGrid()
        -- Logger.warn("Grid populated.")
    end
    
    local start = getNearestGridPoint(startPos)
    local target = getNearestGridPoint(targetPos)

    local path = astar(start, target, manhattan, dangerousness)

    return path
end