local sensorInfo = {
	name = "FormationCustomGroup",
	desc = "invert table of unitIDs.",
	author = "Jakub Mestek",
	date = "2017-08-09",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


-- @description return static position of the first unit
return function(unitsIDs)
	result = {}
	for idx, unitID in ipairs(unitsIDs) do
		result[unitID] = idx
	end
	return result
end