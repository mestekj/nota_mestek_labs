local sensorInfo = {
	name = "From1D",
	desc = "Return 3D position coresponding to 1D coordinate on given path.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return 3d pos, path is list of Vec3
return function(path, pos1d)
	local remains = pos1d
	for i = 2, #path do
		local stepLength = (path[i] - path[i-1]):Length()
		remains = remains - stepLength
		if remains <= 0 then
			local ratio = -remains / stepLength
			local pos3d =  path[i-1] * (ratio) +  path[i] * (1-ratio)
			return pos3d
		end
	end
	-- pos1d is behind last vertex, return its coords
	return path[#path]
end