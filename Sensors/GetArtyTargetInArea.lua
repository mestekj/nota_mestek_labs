local sensorInfo = {
	name = "GetEnemyBuildingInArea",
	desc = "Returns building (static unit) in given area (if any)",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetUnitDefID = Spring.GetUnitDefID
local UnitPosition = Sensors.nota_mestek_labs.UnitPosition

-- @description return a static enemy unit
return function(area)
	local enemyUnits = Sensors.nota_mestek_labs.EnemyUnitsInArea(area)

	maxHPID = nil
	maxHP = 0

	maxHPIDB = nil
	maxHPB = 0

	closestID = nil
	closestDist = math.huge

	for _, unitID in ipairs(enemyUnits) do
		local unitDef = UnitDefs[SpringGetUnitDefID(unitID)]
		--global.enUnitDefs[unitID] = unitDef.name
		if unitDef ~= nil then
			if unitDef.isImmobile or unitDef.isBuilding then
				if unitDef.health > maxHPB then
					maxHPB = unitDef.health
					maxHPIDB = unitID
				end
				
				local distance = (UnitPosition(unitID) - Vec3(1000,0,9000)):Length()
				if distance < closestDist then
					closestID = unitID
					closestDist = distance
				end

			elseif unitDef.health > maxHP then
				maxHP = unitDef.health
				maxHPID = unitID
			end
		end
	end
	if maxHPIDB ~= nil then
		if maxHPIDB > 2000 then
			return maxHPIDB
		else
			return closestID
		end
	elseif maxHPID ~= nil then
		return maxHPID
	else
		return enemyUnits[1]
	end
end