local sensorInfo = {
	name = "GetEnemyBuildingInArea",
	desc = "Returns building (static unit) in given area (if any)",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetUnitDefID = Spring.GetUnitDefID

-- @description return a static enemy unit
return function(area)
	local enemyUnits = Sensors.nota_mestek_labs.EnemyUnitsInArea(area)

	local buildings = {}

	for _, unitID in ipairs(enemyUnits) do
		local unitDef = UnitDefs[SpringGetUnitDefID(unitID)]
		--global.enUnitDefs[unitID] = unitDef.name
		if unitDef ~= nil then
			if unitDef.isImmobile or unitDef.isBuilding then
				table.insert(buildings, Sensors.nota_mestek_labs.UnitPosition(unitID))
			end
		end
	end
	return buildings
end