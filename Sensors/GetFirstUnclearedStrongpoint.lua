local sensorInfo = {
	name = "GetFirstEnemyStrongpoint",
	desc = "Return position of nearest enemy stronghold",
	author = "Jakub Mestek",
	date = "2021-08-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGetUnitDefID = Spring.GetUnitDefID

function getStatus(position)
	local enemyUnits = Sensors.nota_mestek_labs.EnemyUnitsInArea({center = position, radius=600})
	local isShika = false
	local isBuilding = false
	local isNotKnown = false
	for _, unitID in ipairs(enemyUnits) do
		local unitDef = UnitDefs[SpringGetUnitDefID(unitID)]

		if unitDef == nil then
			isNotKnown = true
		elseif unitDef.isImmobile or unitDef.isBuilding then
			-- return {isCleared = false, revealed = true}
			isBuilding = true
			if unitDef.name == 'shika' then
				isShika = true
			end
		end
	end

	if isBuilding then
		return {isCleared = false, revealed = true, shikaDown = not isShika}
	elseif isNotKnown then
		return {isCleared = false, revealed = false, shikaDown = false}
	else
		return {isCleared = true, revealed = true, shikaDown = true}
	end
end

-- @description return findMin function
return function(corridor)
	local myAllyID = 0
	local previousCleared = false
	for _, val in ipairs(corridor.points) do
		if val.isStrongpoint == true then
			local status = getStatus(val.position)
			if not status.isCleared then
				return {position =  val.position, revealed = status.revealed, shikaDown = status.shikaDown, enemyBase = false}
			elseif val.ownerAllyID ~= myAllyID and not Spring.IsPosInRadar(val.position.x, val.position.y, val.position.z, myAllyID) and not Spring.IsPosInLos(val.position.x, val.position.y, val.position.z, myAllyID) then
				return {position =  val.position, revealed = false, shikaDown = false, enemyBase = false}
			end
		end
	end
	local enemyBase = corridor.points[#corridor.points]
	return { position = enemyBase.position, revealed = false, shikaDown = true, enemyBase = true}
end