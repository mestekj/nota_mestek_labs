local sensorInfo = {
	name = "GetMyMetal",
	desc = "Returns current level of metal.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return list of my units
return function()
	local myTeamID = Spring.GetLocalTeamID()
	local metal = Spring.GetTeamResources(myTeamID, "metal")
	return metal
end