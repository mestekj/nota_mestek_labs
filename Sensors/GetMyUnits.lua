local sensorInfo = {
	name = "GetMyUnits",
	desc = "Returns list of all units of my team (not my alliance).",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return list of my units
return function()
	local myTeamID = Spring.GetLocalTeamID()
	return Spring.GetTeamUnits(myTeamID)
end