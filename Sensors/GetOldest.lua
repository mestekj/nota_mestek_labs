local sensorInfo = {
	name = "GetOldest",
	desc = "Return oldest unit.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return 3d pos, path is list of Vec3
return function(units)
	oldest = math.huge
	for _, unitID in ipairs(units) do
		if unitID < oldest then
			oldest = unitID
		end
	end
	return oldest
end