local sensorInfo = {
	name = "GetSmallestGroup",
	desc = "Return ID of smallest group from given groupsIDs",
	author = "Jakub Mestek",
	date = "2021-07-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description assign unit
return function(groupIDs, groups)
	local cmin = nil
    local cmin_key = math.huge
    for i, groupID in ipairs(groupIDs) do
        local k = #groups[groupID]
        if k < cmin_key then
            cmin_key = k
            cmin = groupID
        end
    end
    return cmin
end