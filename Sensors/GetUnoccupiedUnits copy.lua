local sensorInfo = {
	name = "GetUnoccupiedUnits",
	desc = "Get unoccupied units.",
	author = "Jakub Mestek",
	date = "2021-07-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return get units
return function(unitsIDs)
	local result = {}
    for _,unitID in ipairs(myUnits) do
        if UnitDefs[Spring.GetUnitDefID(unitID)].name == name then
			table.insert(result, unitID)
		end
    end
    return result
end