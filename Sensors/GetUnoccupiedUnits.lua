local sensorInfo = {
	name = "GetUnoccupiedUnits",
	desc = "Get unoccupied units by categories.",
	author = "Jakub Mestek",
	date = "2021-07-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function IsOccupied(unitID, groupAssignment)
	return groupAssignment[unitID] ~= nil
end

-- @description return get units
return function(groupAssignment)
	local myTeamID = Spring.GetLocalTeamID()
	local myUnits =  Spring.GetTeamUnits(myTeamID)

	local noUnits = true
	local result = {}
    for _,unitID in ipairs(myUnits) do
        if not IsOccupied(unitID, groupAssignment) then
			local name = UnitDefs[Spring.GetUnitDefID(unitID)].name
			if result[name] == nil then
				result[name] = {unitID}
				noUnits = false
			else
				table.insert(result[name], unitID)
			end
		end
    end
	if noUnits then
		return nil
	else
    	return result
	end
end