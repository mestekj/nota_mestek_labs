local sensorInfo = {
	name = "GetUnoccupiedUnitsByName",
	desc = "Filter lua table (array).",
	author = "Jakub Mestek",
	date = "2021-07-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function IsOccupied(unitID, groupAssignment)
	return groupAssignment[unitID] == nil
end

-- @description return filter function
return function(groupAssignment, name)
	local myTeamID = Spring.GetLocalTeamID()
	local myUnits =  Spring.GetTeamUnits(myTeamID)

	local result = {}
    for _,unitID in ipairs(myUnits) do
        if not IsOccupied(unitID, groupAssignment) and UnitDefs[Spring.GetUnitDefID(unitID)].name == name then
			table.insert(result, unitID)
		end
    end
    return result
end