local sensorInfo = {
	name = "InitGroups",
	desc = "Init given number of groups.",
	author = "Jakub Mestek",
	date = "2021-07-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


-- @description return findMin function
return function(number)
	local groups = {}
	for i = 1, number do
		groups[i] = {}
	end
	return groups
end