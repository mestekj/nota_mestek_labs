local sensorInfo = {
	name = "UnitPosition",
	desc = "Return position of given unit.",
	author = "Jakub Mestek",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function(stuckData, unitID, limit)
	local x,y,z = Spring.GetUnitPosition(unitID)
	local pos = Vec3(x,y,z)
	local time = Spring.GetGameSeconds()
	local update = false
	if stuckData == nil then
		stuckData = {}
	end
	if stuckData.pos == nil then
		update = true
	elseif (pos - stuckData.pos):Length() > 100 then
		update = true
	end
	if update then
		stuckData.time = time
		stuckData.pos = pos
		stuckData.isStuck = false
	elseif stuckData.time > time + limit*60 then
		stuckData.isStuck = true
	end
	return stuckData.isStuck
end