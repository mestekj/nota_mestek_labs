local sensorInfo = {
	name = "MetalInArea",
	desc = "Computes number of metal in given area.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return total number of metal in stones or scrap
return function(area)
	local features = Spring.GetFeaturesInCylinder(area.center.x, area.center.z, area.radius)
	local total = 0
	for _, featureID in ipairs(features) do
		local remainingMetal, _ = Spring.GetFeatureResources(featureID)
		total = total + remainingMetal
	end
	return total
end