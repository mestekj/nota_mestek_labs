local sensorInfo = {
	name = "ParseCorridors",
	desc = "Parse MissionInfo.corridors to only lists of Vec3",
	author = "Jakub Mestek",
	date = "2021-07-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function parseCorridor(corridor)
    local parsed = {}
    for idx,val in ipairs(corridor.points) do
		if val.isStrongpoint or idx == 1 or idx == #corridor.points then
        	table.insert(parsed, val.position)
		end
    end
    return parsed
end

-- @description return findMin function
return function(corridors)
	return {Top = parseCorridor(corridors.Top), Middle = parseCorridor(corridors.Middle), Bottom = parseCorridor(corridors.Bottom)}
end