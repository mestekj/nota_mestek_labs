local sensorInfo = {
	name = "ReservationSystem",
	desc = "Reservation system of jobs and workers.",
	author = "Jakub Mestek",
	date = "2021-07-18",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local rs = {}
local rs_mt = { __index = rs }
rs_mt.__metatable = false -- disable accessing of the metatable

local function new(workers, jobs)
    return setmetatable({workersList = workers, jobsList=jobs, workersAssignment = {}, jobsAssignment = {}, unfinishedJobs = #jobs, livingWorkers = #workers}, rs_mt)
end

function rs:GetUnoccupiedWorker()
    for _, worker in ipairs(self.workersList) do
        if self.workersAssignment[worker] == nil then
            return worker
        end
    end
    return nil
end

function rs:GetOpenJob()
    for _, job in ipairs(self.jobsList) do
        if self.jobsAssignment[job] == nil then
            return job
        end
    end
    return nil
end

function rs:Assign(worker, job)
    self.workersAssignment[worker] = job
    self.jobsAssignment[job] = worker
end

function rs:GetPairToProcess()
    worker = self:GetUnoccupiedWorker()
    job = self:GetOpenJob()
    if worker == nil or job == nil then
        return nil
    end

    self:Assign(worker,job)
    return {worker = worker, job = job}
end

function rs:JobDone(job)
    worker = self.jobsAssignment[job]
    self.jobsAssignment[job] = "done"
    self.unfinishedJobs = self.unfinishedJobs - 1
    self.workersAssignment[worker] = nil
end

function rs:WorkerDied(worker)
    local job = self.workersAssignment[worker]
    if job and self.jobsAssignment[job] ~= "impossible" then
        self.jobsAssignment[job] = nil
    end
    self.workersAssignment[worker] = "dead"
    self.livingWorkers = self.livingWorkers - 1
end

function rs:JobImpossible(job)
    local worker = self.jobsAssignment[job]
    if worker and self.workersAssignment[worker] ~= "dead" then
        self.workersAssignment[worker] = nil
    end
    self.jobsAssignment[job] = "impossible"
    self.unfinishedJobs = self.unfinishedJobs - 1
end

function rs:Unassign(worker,job)
    self.jobsAssignment[job] = nil
    self.workersAssignment[worker] = nil
end

function rs:Finished()
    return self.unfinishedJobs == 0 or self.livingWorkers == 0
end

function rs:CheckJobs(safeArea)
    for _, job in ipairs(self.jobsList) do
        local x,y,z = Spring.GetUnitPosition(job)
	    local pos = Vec3(x,y,z)
        local dist = (pos - safeArea.center):Length()
        if dist > safeArea.radius then
            self.jobsAssignment[job] = nil
            self.unfinishedJobs = self.unfinishedJobs + 1
        end
    end
end

-- @description create new RS
return new