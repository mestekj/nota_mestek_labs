local sensorInfo = {
	name = "To1D",
	desc = "Return 1D coordinate on on given path coresponding to 3D  position.",
	author = "Jakub Mestek",
	date = "2021-07-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

--[[
function FindNearest(array, pos3d)
	local cmin = nil
    local cmin_key = math.huge
    local cmin_index = nil
    for i, val in ipairs(array) do
        local k = (pos3d - val):Length()
        if k < cmin_key then
            cmin_key = k
            cmin = val
            cmin_index = i
        end
    end
    return cmin_index, cmin
end
]]

-- @description return 1d pos, path is list of Vec3, pos3d is Vec3
return function(path, pos3d)
	local pos1d = 0

	local i = 2
	local toCurr = (path[i] - pos3d):Length()
	local fromPrev = (pos3d - path[i-1]):Length()

	if toCurr > fromPrev then
		local stepLength = (path[i] - path[i-1]):Length()
		local ratio = fromPrev / (fromPrev + toCurr)
		pos1d = pos1d + ratio * stepLength
		return pos1d
	end

	for i = 2, #path-1 do
		local stepLength = (path[i] - path[i-1]):Length()
		local toNext = (path[i+1] - pos3d):Length()
		local toCurr = (path[i] - pos3d):Length()
		local fromPrev = (pos3d - path[i-1]):Length()
		-- if fromPrev <= stepLength and toNext <= stepLength then
		if toCurr <= fromPrev and toCurr <= toNext then
			if toNext < fromPrev then
				pos1d = pos1d + stepLength
				fromPrev = toCurr
				toCurr = toNext
				i = i+1
			end
			local ratio = fromPrev / (fromPrev + toCurr)
			pos1d = pos1d + ratio * stepLength
			return pos1d
		else
			pos1d = pos1d + stepLength
		end
	end

	-- pos3d is behind last but one vertex
	local i = #path
	local stepLength = (path[i] - path[i-1]):Length()
	local toCurr = (path[i] - pos3d):Length()
	local fromPrev = (pos3d - path[i-1]):Length()
	local ratio = fromPrev / (fromPrev + toCurr)
	pos1d = pos1d + ratio * stepLength
	return pos1d



	--[[

	local nearestPoint, _ = FindNearest(path, pos3d)
	local secondNearest = 0
	if nearestPoint == 1 then
		secondNearest = 1
	elseif nearestPoint == #path then
		secondNearest = #path
	else
		local toNext = (path[nearestPoint + 1] - pos3d):Length()
		local fromPrev = (pos3d - path[nearestPoint-1]):Length()
		if toNext < fromPrev then
			secondNearest = nearestPoint + 1
		else
			secondNearest = nearestPoint - 1
		end
	end
	
	if nearestPoint < secondNearest then
		local s = nearestPoint
		nearestPoint = secondNearest
		secondNearest = s
	end

	local pos1d = 0

	for i = 2, secondNearest do
		local stepLength = (path[i] - path[i-1]):Length()
		pos1d = pos1d + stepLength
	end

	if secondNearest ~= nearestPoint then
		local toNext = (path[nearestPoint] - pos3d):Length()
		local fromPrev = (pos3d - path[secondNearest]):Length()
		local ratio = fromPrev / (fromPrev + toNext)
		local stepLength = (path[nearestPoint] - path[secondNearest]):Length()
		pos1d = pos1d + stepLength * ratio
	end
	return pos1d, secondNearest

	]]
end