local sensorInfo = {
	name = "UnitGroupPosition",
	desc = "Return position of given group of units.",
	author = "Jakub Mestek",
	date = "2017-08-09",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local UnitPosition = Sensors.nota_mestek_labs.UnitPosition

-- @description return static position of the first unit
return function(unitsIDs, target3d)
	target3d = target3d or Vec3(10000,0,0)
	local minDist = math.huge
	local minPos = nil
	for _,unitID in ipairs(unitsIDs) do
		local pos = UnitPosition(unitID)
		local dist = (target3d - pos):Length() 
		if dist < minDist then
			minDist = dist
			minPos = pos
		end
	end
	return minPos
end