local sensorInfo = {
	name = "UnitPosition",
	desc = "Return position of given unit.",
	author = "Jakub Mestek",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return static position of the first unit
return function(unitID)
	local x,y,z = SpringGetUnitPosition(unitID)
	return Vec3(x,y,z)
end