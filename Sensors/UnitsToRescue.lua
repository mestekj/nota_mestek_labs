local sensorInfo = {
	name = "UnitsToRescue",
	desc = "Return list of all rescuable units in map, ordered by distance from safeArea.",
	author = "Jakub Mestek",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function filter(array, predicate)
    result = {}
    for _,val in ipairs(array) do
        if predicate(val) then table.insert(result, val) end
    end
    return result
end

function findMin(array, key_fn)
    local cmin = nil
    local cmin_key = math.huge
    local cmin_index = nil
    for i, val in ipairs(array) do
        k = key_fn(val)
        if k < cmin_key then
            cmin_key = k
            cmin = val
            cmin_index = i
        end
    end
    return cmin_index, cmin
end

function sort(array, key_fn)
	local sorted_array = {}
	for i = 1, #array do
		local idx, val = findMin(array, key_fn)
		table.insert(sorted_array, val)
		table.remove(array, idx)
	end
	return sorted_array
end

function isTransportable(unitID)
	return not UnitDefs[Spring.GetUnitDefID(unitID)].cantBeTransported
end


function distanceToSafeArea(unitID,safeArea)
	local x,y,z = Spring.GetUnitPosition(unitID)
	local unitPos = Vec3(x,y,z)
	return (unitPos - safeArea.center):Length()
end

-- @description return static position of the first unit
return function(safeArea)
	local myTeam = Spring.GetLocalTeamID()
	local allUnits = Spring.GetTeamUnits(myTeam)
	local transportableUnits = filter(allUnits, isTransportable)
	local sorted = sort(transportableUnits, function (x) return distanceToSafeArea(x, safeArea) end)
	return sorted
end