local sensorInfo = {
	name = "WindDebug",
	desc = "Draws an arrow showing wind strength and direction.",
	author = "Jakub Mestek",
	date = "2021-05-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description draw arrow indicating wind
return function(wind, length_multiplier)
	if #units > 0 then
		local unitID = units[1]
		local x,y,z = Spring.GetUnitPosition(unitID)
		local wind_vec = Vec3(wind.dirX, 0, wind.dirZ)
		wind_vec = wind_vec * length_multiplier
		if (Script.LuaUI('exampleDebug_update')) then
			Sensors.nota_mestek_labs.DrawArrow(
				unitID, -- key
				{	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(x,y,z) + wind_vec
				}
			)
		end
		return {	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(x,y,z) + wind_vec
				}
	end
end